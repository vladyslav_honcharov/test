import os
import re
def rm_merged_remotes():
    with open('var/merged', 'r') as f:
        for line in f.readlines():
            branch = line.split('/')[-1].strip()
            os.system(f'git push origin :{branch}')
def merge():
    with open('to_merge', 'r') as f:
        for line in f.readlines():
            match = re.search(r'(.+?)\s+.+?$', line)
            if match:
                branch = match.group(1)
                os.system(f'git merge {branch}')
if __name__ == '__main__':
    # rm_merged_remotes()
    merge()

